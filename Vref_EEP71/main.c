/*
 * EEP71_Vref.c
 *
 * Created: 1/14/2020 9:29:06 AM
 * Author : Mitchell.K
 */ 

#define F_CPU 16000000UL
#define BAUD 9600
#define BRC ((F_CPU/16/BAUD)-1 )     //  baud rate calculatie

////
#include <stdlib.h> // ITOA function
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
//#include "uart.h"
///// 

char buffer[5];            // output array [5] included 'o' char from string 
float adc_value;        // store convert value
uint8_t x = 0;	// for



//////////////////////////////////////////////////////////////////////////
// function declecration 
float ADC_read(uint8_t port);
void ADC_init(void);
void usart_init(void);
void usart_send(unsigned char data);
void usart_string(char* StringPtr);
unsigned char usart_receive(void);
//////////////////////////////////////////////////////////////////////////
double Vold;
double Iold;
double Vmax;
double dcv;
double I;
double V1,V2;
// kalibratie waardes voor de stroom sensor
uint16_t sensitivity = 100;
uint16_t offsetvolt = 2500;

int main(void)
{	
	ADC_init();  //setup
	usart_init();

	while(1)
	{	
		//usart_string("\n");
		//for (x=0; x < 6; x++) // passing by port ADMUX
		//{
		//usart_string("Reading PORT ");
		//usart_send('0' + x);            //Nr 0-9
		//usart_string(": ");            // space
		//adc_value = ADC_read(x);        // read ADC port
		////adc_value = (adc_value*5)/1023;
		//itoa(adc_value, buffer, 10);        //Convert ADC to Ascii itoa(value,output,decimaal)
		//usart_string(buffer);        // send buffer TX
		//usart_string("\n");            //new line
		//_delay_ms(500);                //delay read
		//}
		//"Hier moet dus de pinspanning = (adcwaarde/1023)*5000 dus het pin A3 of A4 om de adc te sampelen"
		//"daarna I = (pin spanning - offsetvolt)/sensitivity"
		//"daarna kan de functie geroepen worden  variabel = Vref(V,I);"
		//"en natuurlijk V is het voltage van de spanning deler "
	}
		
}

uint16_t D_CV(double Vpp,double Vfb){
	double delta = 0.05;
	if (V1 == V2) // als de spanning gelijk zijn wordt de gemeten terug gegeven
	{
		return dcv;
	}
	else if (V1 > V2) // als Vpaneel groter is dan Vfb wordt er een compensatie bijgevoegd voor de duty cycle
	{
		dcv = dcv + delta;
	}
	else{
		dcv = dcv - delta; // anders wordt een factor afgetrokken van de duty cycle
	}
	// geheugen per iteratie 
	V1 =Vpp;
	V2 =Vfb;
	return dcv;
}
double Vref(double V , double I)
{	
	
	uint8_t delta = 1;
	//geheugen per iteratie
	double dV = V-Vold;
	double dI = I-Iold;
	if (dV > 0) // check als verandering in spanning groter is dan 0
	{
		if (dI == 0) // check als de verandering in de stroom is gelijk aan "0"
		{
			Vmax = V;
		}
		else if (dI>0) // check als de verandering in de stroom is groter dan 0
		{
			Vmax = Vold + delta;
		}
		else{
			Vmax = Vold - delta;
		}
	}
	else{
		if (dI/dV == I/V) // compare instantaneous en incremental conductances, is het gelijk return V
		{
			Vmax = V;
		}
		else if (dI/dV > I/V)
		{
			Vmax = Vold + delta; // als instantaneous groter is dan incremental vergroot de spanning anders niet
		}
		else{
			Vmax = Vold - delta;
		}
	}
	Vold = V;
	Iold = I;
	return Vmax;
}



void usart_init(void)
{
	// shift data BRC in UBRR0 1byte and 1 nibble
	UBRR0H = (BRC >> 8);
	UBRR0L = BRC;
	UCSR0B = (1<<TXEN0); // TX enable
	UCSR0C = (1<<UCSZ01) | (1<<UCSZ00); // nr of data in frame(8)
}
void usart_send( unsigned char data){
	
	while(!(UCSR0A & (1<<UDRE0))); // wait tot empty buffer
	UDR0 = data; // zet data in buffer en verstuur
	
}
void usart_string(char* StringPtr){
	// check 'o' char aan het eind ,, to see if more char need to be send
	while(*StringPtr != 0x00)
	{
		usart_send(*StringPtr); // send char 
		StringPtr++; // increment om de vlgende char te lezen
	}
}
void ADC_init(void){
	
	ADCSRA |= ((1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0));    //16Mhz/128 = 125Khz clockspeed
	ADMUX |= (1<<REFS0);                //Voltage ref (5v)
	ADCSRA |= (1<<ADEN);                //Turn on ADC
	ADCSRA |= (1<<ADSC);                //Eerste Conversie test
}
float ADC_read(uint8_t port){
	ADMUX &= 0xF0;                    //clear port
	ADMUX |= port;                // selecter nieuwe port
	ADCSRA |= (1<<ADSC);                //Starts a new conversie
	while(ADCSRA & (1<<ADSC));            //wacht tot conversie klaar
	return ADCW;
}
